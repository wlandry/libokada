import waflib.Tools.fc_config

def options(opt):
    opt.load('compiler_cxx compiler_c compiler_fc gnu_dirs FTensor')

def configure(conf):
    configure_variant(conf);

def configure_variant(conf):
    conf.load('compiler_cxx compiler_c compiler_fc gnu_dirs FTensor')
    conf.check_fortran_dummy_main()
    conf.check_fortran_mangling()
    
    # Optimization flags
    optimize_msg="Checking for optimization flag "
    optimize_fragment="int main() {}\n"
    optimize_flags=['-Ofast','-O3','-O2','-O']
    for flag in optimize_flags:
        try:
            conf.check_cxx(msg=optimize_msg+flag, fragment=optimize_fragment,
                           cxxflags=flag, uselib_store='optimize')
        except conf.errors.ConfigurationError:
            continue
        else:
            found_optimize=True
            break

    # conf.mangle_name(FORTRAN_MANGLING,'foo_bar')
        
def build(bld):
    default_flags=['-Wall', '-Wextra', '-Wconversion']
    
    use_array=['optimize','FTensor']

    okada_sources=['src/okada/Okada/Okada.cxx',
                   'src/okada/Okada/dc3d.cxx',
                   'src/okada/Okada/displacement.cxx',
                   'src/okada/Okada/source_contribution.cxx',
                   'src/okada/Okada/Iteration_Params/Iteration_Params.cxx',
                   'src/okada/Okada/Iteration_Params/ua.cxx',
                   'src/okada/Okada/Iteration_Params/ub.cxx',
                   'src/okada/Okada/Iteration_Params/uc.cxx',
                   'src/okada/Triangle_Fault/Triangle_Fault.cxx',
                   'src/okada/Triangle_Fault/displacement/displacement.cxx',
                   'src/okada/Triangle_Fault/displacement/dislocation_displacement/dislocation_displacement.cxx',
                   'src/okada/Triangle_Fault/displacement/dislocation_displacement/trimode_finder.cxx',
                   'src/okada/Triangle_Fault/displacement/dislocation_displacement/angular_dislocation/angular_dislocation.cxx',
                   'src/okada/Triangle_Fault/displacement/dislocation_displacement/angular_dislocation/angular_dislocation_unrotated.cxx',
                   'src/okada/Triangle_Fault/displacement/harmonic_displacement/harmonic_displacement.cxx',
                   'src/okada/Triangle_Fault/displacement/harmonic_displacement/free_surface_correction/free_surface_correction.cxx',
                   'src/okada/Triangle_Fault/displacement/harmonic_displacement/free_surface_correction/harmonic_correction_displacement.cxx',
                   'src/okada/Triangle_Fault/displacement/harmonic_displacement/free_surface_correction/harmonic_correction_strain.cxx',
                   'src/okada/Strain_Volume/Strain_Volume.cxx',
                   'src/okada/Strain_Volume/displacement/displacement.cxx',
                   'src/okada/Strain_Volume/displacement/IU.cxx',
                   'src/okada/Strain_Volume/displacement/dIU.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxxx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxxy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxxz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxyx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxyy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxyz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxzx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxzy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jxzz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jzxx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jzxy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jzxz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jzzx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/Jzzz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJ.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxxx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxxy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxxz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxyx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxyy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxyz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxzx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxzy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJxzz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJzxx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJzxy.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJzxz.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJzzx.cxx',
                   'src/okada/Strain_Volume/displacement/Integrals/dJ/dJzzz.cxx']
    
    bld.stlib(
        source=okada_sources,
        target='okada',
        name='okada_st',
        install_path=bld.env.LIBDIR,
        cxxflags = default_flags,
        use=use_array
        )

    bld.shlib(
        source=okada_sources,
        target='okada',
        install_path=bld.env.LIBDIR,
        cxxflags = default_flags,
        use=use_array,
        vnum='1.1.0'
        )

    bld.shlib(
        source=['src/okada_c.cxx'],
        target='okada_c',
        name='libokada_c',
        install_path=bld.env.LIBDIR,
        cxxflags = default_flags + ['-DOKADA_DISPLACEMENT_FORTRAN='
                                    + waflib.Tools.fc_config.mangle_name
                                    (bld.env.FORTRAN_MANGLING[0],
                                     bld.env.FORTRAN_MANGLING[1],
                                     bld.env.FORTRAN_MANGLING[2],
                                     'okada_displacement_f'),
                                    '-DTRIANGLE_DISPLACEMENT_FORTRAN='
                                    + waflib.Tools.fc_config.mangle_name
                                    (bld.env.FORTRAN_MANGLING[0],
                                     bld.env.FORTRAN_MANGLING[1],
                                     bld.env.FORTRAN_MANGLING[2],
                                     'triangle_displacement_f'),
                                    '-DSTRAIN_VOLUME_DISPLACEMENT_FORTRAN='
                                    + waflib.Tools.fc_config.mangle_name
                                    (bld.env.FORTRAN_MANGLING[0],
                                     bld.env.FORTRAN_MANGLING[1],
                                     bld.env.FORTRAN_MANGLING[2],
                                     'strain_volume_displacement_f')],
                                    
        use=use_array,
        vnum='1.1.0'
        )

    bld.install_files(bld.env.INCLUDEDIR,['src/okada.hxx'])
    bld.install_files(bld.env.INCLUDEDIR+'/okada',
                      ['src/okada/Okada.hxx',
                       'src/okada/Triangle_Fault.hxx',
                       'src/okada/Displacement.hxx',
                       'src/okada/Strain_Volume.hxx',
                       'src/okada/radians.hxx'])

    bld.program(
        features=['c','fc','fcprogram'],
        source=['test/okada/fortran.f90', 'test/okada/okada.f90',
                'test/okada/DC3D.f'],
        use=['optimize'],
        fcflags=default_flags,
        target='okada_original_fortran',
        install_path=None
        )

    bld.program(
        source=['test/okada/okada_cxx.cxx'],
        target='okada_cxx',
        use=['okada_st','FTensor','optimize'],
        cxxflags=default_flags,
        install_path=None
        )

    bld.program(
        features=['c','cprogram'],
        source=['test/okada/okada_c.c'],
        target='okada_c',
        use=['libokada_c','okada','optimize'],
        rpath=['build'],
        install_path=None
        )

    bld.program(
        features=['fc','fcprogram'],
        source=['test/okada/okada_f90.f90'],
        target='okada_f90',
        use=['libokada_c','okada','optimize'],
        rpath=['build'],
        install_path=None
        )

    bld.program(
        source=['test/triangle/triangle_cxx.cxx'],
        target='triangle_cxx',
        use=['okada_st','FTensor','optimize'],
        cxxflags=default_flags,
        install_path=None
        )
    
    bld.program(
        features=['c','cprogram'],
        source=['test/triangle/triangle_c.c'],
        target='triangle_c',
        use=['libokada_c','okada','optimize'],
        rpath=['build'],
        install_path=None
        )

    bld.program(
        features=['fc','fcprogram'],
        source=['test/triangle/triangle_f90.f90'],
        target='triangle_f90',
        use=['libokada_c','okada','optimize'],
        rpath=['build'],
        install_path=None
        )

    bld.program(
        source=['test/strain_volume/strain_volume_cxx.cxx'],
        target='strain_volume_cxx',
        use=['okada_st','FTensor','optimize'],
        cxxflags=default_flags,
        install_path=None
        )
    
    bld.program(
        features=['c','cprogram'],
        source=['test/strain_volume/strain_volume_c.c'],
        target='strain_volume_c',
        use=['libokada_c','okada','optimize'],
        rpath=['build'],
        install_path=None
        )

    bld.program(
        features=['fc','fcprogram'],
        source=['test/strain_volume/strain_volume_f90.f90'],
        target='strain_volume_f90',
        use=['libokada_c','okada','optimize'],
        rpath=['build'],
        install_path=None
        )

