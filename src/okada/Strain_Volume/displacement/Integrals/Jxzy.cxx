#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jxzy() const
  {
    const double x3(xyz(2)), y3(y(2));
    return -((r1+(1/r2)*((7+8*((-2)+nu)*nu)*r2*r2+2*x3*y3)
                   + xLogy(2*((-3)*x3-2*y3+6*nu*(x3+y3)
                              -4*nu*nu*(x3+y3)),r2+x3+y3))
             + 2*((-3)+4*nu)*x3*acoth((1/r2)*(x3+y3)));
  }
}
