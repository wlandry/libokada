#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jzzz() const
  {
    const int ix(0), iy(1-ix);
    const double x1(xyz(ix)), x2(xyz(iy)), x3(xyz(2)), y1(y(ix)), y2(y(iy)), y3(y(2));

    return 2*(1/r2)*x3*(x1-y1)*(x2-y2)*y3
      * (1/((x1-y1)*(x1-y1)+(x3+y3)*(x3+y3)))
      * (1/((x2-y2)*(x2-y2)+(x3+y3)*(x3+y3)))
      * ((x1-y1)*(x1-y1)+(x2-y2)*(x2-y2)+2*(x3+y3)*(x3+y3))
      - 3*x3*atan2(3*x3,x1-y1)
      - 5*x3*atan2(5*x3,x2-y2)
      + 12*nu*x3*atan2((-3)*nu*x3,x2-y2)
      + 4*nu*x3*atan2(-nu*x3,x1-y1)
      - 8*nu*nu*x3*atan2(nu*nu*x3,x2-y2)
      + 3*y3*atan2((-3)*y3,x1-y1)
      - 5*y3*atan2(5*y3,x2-y2)
      + 12*nu*y3*atan2((-3)*nu*y3,x2-y2)
      - 4*nu*y3*atan2(nu*y3,x1-y1)
      - 8*nu*nu*y3*atan2(nu*nu*y3,x2-y2)
      + 2*((-1)+2*nu)*(x3+(-1)*y3)*atan2(r1*(-x3+y3),(x1-y1)*(x2-y2))
      + 2*(1-2*nu)*(1-2*nu)*(x3+y3)*atan2(r2*(x3+y3),(x1-y1)*(x2-y2))
      + xLogy(-((-3)+4*nu)*(x1-y1),r1+x2-y2)
      + xLogy((5+4*nu*((-3)+2*nu))*(x1-y1),r2+x2-y2)
      + xLogy(-((-3)+4*nu)*(x2-y2),r1+x1-y1)
      + xLogy((5+4*nu*((-3)+2*nu))*(x2-y2),r2+x1-y1);
  }
}
