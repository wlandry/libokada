#include "../../Integrals.hxx"

namespace okada
{
FTensor::Tensor1<double, 3> Integrals::dJxyz (const int &ix) const
{
  const int iy (1 - ix);
  const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2),
      dp3 (x3 + y3), 
      r2xy3p_inv (1 / (r2 + x3 + y3)),
      r1_inv (1 / r1), r2_inv (1 / r2),
    dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5));

  FTensor::Tensor1<double, 3> result;
  /// d/dx
  result (ix)
    = (1 + 8 * (nu - 1) * nu) * r2_inv * d1 + r1_inv * (-x1 + y1)
                   - 4 * (nu - 1) * ((-1) + 2 * nu) * r2_inv * d1 * dp3
                     * r2xy3p_inv + 2 * x3 * d1 * y3 * dr123p_32;
  /// d/dy
  result (iy)
    = (1 + 8 * (nu - 1) * nu) * r2_inv * d2 + r1_inv * (-x2 + y2)
                   - 4 * (nu - 1) * ((-1) + 2 * nu) * r2_inv * d2 * dp3
                     * r2xy3p_inv + 2 * x3 * d2 * y3 * dr123p_32;
  /// d/dz
  result (2)
    = r1_inv * (-x3 + y3) - 4 * (nu - 1) * ((-1) + 2 * nu) * dp3 * r2xy3p_inv
        + (-4) * (nu - 1) * ((-1) + 2 * nu) * r2_inv * dp3 * dp3 * r2xy3p_inv
        + r2_inv * (x3 - y3 - 8 * nu * dp3 + 8 * nu * nu * dp3)
        + 2 * x3 * y3 * dp3 * dr123p_32
        + xLogy ((-4) * (nu - 1) * ((-1) + 2 * nu), r2 + x3 + y3);
  return result;
}
}
