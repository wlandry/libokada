#include "../../Integrals.hxx"

namespace okada
{
  FTensor::Tensor1<double, 3> Integrals::dJxxx (const int &ix) const
  {
    const int iy (1 - ix);
    const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2), d3 (x3 - y3),
      dp3 (x3 + y3), dr12_inv (1 / (d1 * d1 + d2 * d2)),
      dr13_inv (1 / (d1 * d1 + d3 * d3)), dr13p (d1 * d1 + dp3 * dp3),
      dr13p_inv (1 / dr13p), r1xy2_inv (1 / (r1 + x2 - y2)),
      r1xy3_inv (1 / (r1 + x3 - y3)), r2xy2_inv (1 / (r2 + x2 - y2)),
      r2xy3p_inv (1 / (r2 + x3 + y3)), r1_inv (1 / r1), r2_inv (1 / r2),
      dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5)),
      x1d2 (1 / (x1 * x1 + d2 * d2)), nx1d2 (1 / (9 * x1 * x1 + d2 * d2)),
      nux1d2 (1 / (nu * nu * x1 * x1 + d2 * d2)),
      y1d2 (1 / (y1 * y1 + d2 * d2)), ny1d2 (1 / (9 * y1 * y1 + d2 * d2)),
      nuy1d2 (1 / (nu * nu * y1 * y1 + d2 * d2));

    FTensor::Tensor1<double, 3> result;
    /// d/dx
    result (ix)
      = 2 * (nu - 1) * ((-1) + 2 * nu) * d1 * dr12_inv * d2
        + x1 * x1d2 * (-x2 + y2) + 9 * x1 * nx1d2 * (-x2 + y2)
        + 4 * nu * nu * x1 * nux1d2 * (-x2 + y2)
        + 2 * r2_inv * x3 * (-x1 + y1) / (r2 - x2 + y2)
        - ((-3) + 4 * nu) * r1_inv * d1 * r1xy2_inv * d3
        - 2 * ((-1) + 2 * nu) * r1 * d1 * dr12_inv * d2 * dr13_inv * d3
        - 2 * ((-1) + 2 * nu) * r1_inv * d1 * d1 * d1 * dr12_inv * d2
          * dr13_inv * d3 - ((-3) + 4 * nu) * r1_inv * d1 * d2 * r1xy3_inv
        - (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * r2xy2_inv * dp3
        - (3 - 6 * nu + 4 * nu * nu) * r2_inv * d1 * d2 * r2xy3p_inv
        + 4 * (nu - 1) * ((-1) + 2 * nu) * d1 * dr12_inv * dr12_inv * d2 * y3
          * (2 * x3 + y3)
        - 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * d1 * dr12_inv * d2
          * y3 * (2 * x3 + y3)
        + 4 * (nu - 1) * ((-1) + 2 * nu) * d1 * dr12_inv * d2 * y3
          * (2 * x3 + y3) * dr13p_inv
        - 2 * ((-1) + 2 * nu) * d1 * dr12_inv * d2 * dr13p_inv
          * (((-1) + 2 * nu) * r2 * dp3 + 2 * (nu - 1) * y3 * (2 * x3 + y3))
        + 2 * r2_inv * d1 * dr12_inv * d2 * dr13p_inv
          * (-x1 * x1 * x3 + 2 * x1 * x3 * y1 - x3 * y1 * y1 + 3 * x1 * x1 * y3
             + 2 * x2 * x2 * y3 + 8 * x3 * x3 * y3 - 6 * x1 * y1 * y3
             + 3 * y1 * y1 * y3 - 4 * x2 * y2 * y3 + 2 * y2 * y2 * y3
             + 12 * x3 * y3 * y3 + 4 * y3 * y3 * y3
             + 4 * nu * nu * dp3 * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
             - 2 * nu * dp3 * (4 * d1 * d1 + 3 * (d2 * d2 + 2 * dp3 * dp3)))
        - 4 * r2_inv * d1 * dr12_inv * d2 * dr13p_inv * dr13p_inv
          * (x1 * x1 * x1 * x1 * y3 - 4 * x1 * x1 * x1 * y1 * y3
             - 3 * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             - 2 * x1 * y1 * y3
               * (d2 * d2 + 2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + y3 * (2 * x3 * x3 * x3 * x3 + 7 * x3 * x3 * x3 * y3
                     + (y1 * y1 + y3 * y3) * (y1 * y1 + y2 * y2 + y3 * y3)
                     + x3 * y3 * (6 * y1 * y1 + 3 * y2 * y2 + 5 * y3 * y3)
                     + x3 * x3 * (4 * y1 * y1 + 2 * y2 * y2 + 9 * y3 * y3)
                     + x2 * x2 * (y1 * y1 + dp3 * (2 * x3 + y3))
                     - 2 * x2 * y2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + x1 * x1 * y3
               * (d2 * d2 + 2 * (3 * y1 * y1 + dp3 * (2 * x3 + y3))))
        - 4 * r2_inv * d1 * dr12_inv * dr12_inv * d2 * dr13p_inv
          * (x1 * x1 * x1 * x1 * y3 - 4 * x1 * x1 * x1 * y1 * y3
             - 3 * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             - 2 * x1 * y1 * y3
               * (d2 * d2 + 2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + y3 * (2 * x3 * x3 * x3 * x3 + 7 * x3 * x3 * x3 * y3
                     + (y1 * y1 + y3 * y3) * (y1 * y1 + y2 * y2 + y3 * y3)
                     + x3 * y3 * (6 * y1 * y1 + 3 * y2 * y2 + 5 * y3 * y3)
                     + x3 * x3 * (4 * y1 * y1 + 2 * y2 * y2 + 9 * y3 * y3)
                     + x2 * x2 * (y1 * y1 + dp3 * (2 * x3 + y3))
                     - 2 * x2 * y2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + x1 * x1 * y3
               * (d2 * d2 + 2 * (3 * y1 * y1 + dp3 * (2 * x3 + y3))))
        - 2 * d1 * dr12_inv * d2 * dr13p_inv * dr123p_32
          * (x1 * x1 * x1 * x1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             - 4 * x1 * x1 * x1 * y1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             - 3 * nu * (y1 * y1 + dp3 * dp3)
               * (x3 * x3 * x3 + 3 * x3 * x3 * y3
                  + y3 * (y1 * y1 + y2 * y2 - r2 * y3 + y3 * y3)
                  + x3 * (y1 * y1 + y2 * y2 - 2 * r2 * y3 + 3 * y3 * y3))
             + 2 * nu * nu * (y1 * y1 + dp3 * dp3)
               * (x3 * x3 * x3 + 3 * x3 * x3 * y3
                  + y3 * (y1 * y1 + y2 * y2 - r2 * y3 + y3 * y3)
                  + x3 * (y1 * y1 + y2 * y2 - 2 * r2 * y3 + 3 * y3 * y3))
             + y3 * (2 * x3 * x3 * x3 * x3 + 7 * x3 * x3 * x3 * y3
                     + (y1 * y1 + y3 * y3) * (y1 * y1 + y2 * y2 + y3 * y3)
                     + x3 * y3 * (6 * y1 * y1 + 3 * y2 * y2 + 5 * y3 * y3)
                     + x3 * x3 * (4 * y1 * y1 + 2 * y2 * y2 + 9 * y3 * y3)
                     - r2 * (2 * x3 + y3) * (y1 * y1 + dp3 * dp3))
             + 2 * x2 * y2 * (3 * nu * dp3 * (y1 * y1 + dp3 * dp3)
                              - 2 * nu * nu * dp3 * (y1 * y1 + dp3 * dp3)
                              - y3 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + x2 * x2 * ((-3) * nu * dp3 * (y1 * y1 + dp3 * dp3)
                          + 2 * nu * nu * dp3 * (y1 * y1 + dp3 * dp3)
                          + y3 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + 2 * x1 * y1
               * ((nu - 1) * ((-1) + 2 * nu) * r2 * y3 * (2 * x3 + y3)
                  + x2 * x2 * (-y3 + 3 * nu * dp3 - 2 * nu * nu * dp3)
                  + 2 * x2 * y2 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
                  + 3 * nu * dp3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  - 2 * nu * nu * dp3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  - y3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * (2 * x3 + y3)))
             + x1 * x1
               * (-(nu - 1) * ((-1) + 2 * nu) * r2 * y3 * (2 * x3 + y3)
                  + 2 * x2 * y2 * (-y3 + 3 * nu * dp3 - 2 * nu * nu * dp3)
                  + x2 * x2 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
                  - 3 * nu * dp3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  + 2 * nu * nu * dp3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  + y3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * (2 * x3 + y3))))
        + 2 * (nu - 1) * ((-1) + 2 * nu) * atan (d1 / d2)
        + atan2 (x2 - y2, (-1) * x1) - 3 * atan2 (3 * x1, x2 - y2)
        + 4 * nu * atan2 (-nu * x1, x2 - y2)
        + ((-2) + 4 * nu) * atan2 (r1 * (-x1 + y1), d2 * d3)
        + 2 * (1 - 2 * nu) * (1 - 2 * nu) * atan2 (r2 * (-x1 + y1), d2 * dp3);
    /// d/dy
    result (iy)
      = x1 * x1 * x1d2 + 9 * x1 * x1 * nx1d2 + 4 * nu * nu * x1 * x1 * nux1d2
        - 2 * (nu - 1) * ((-1) + 2 * nu) * d1 * d1 * dr12_inv + y1 * y1 * y1d2
        + 9 * y1 * y1 * ny1d2 + 4 * nu * nu * y1 * y1 * nuy1d2
        + 2 * x3 / (r2 - x2 + y2)
        + 2 * r2_inv * x3 * (-x2 + y2) / (r2 - x2 + y2)
        - ((-3) + 4 * nu) * r1xy2_inv * d3
        - ((-3) + 4 * nu) * r1_inv * d2 * r1xy2_inv * d3
        + 2 * ((-1) + 2 * nu) * r1 * d1 * d1 * dr12_inv * dr13_inv * d3
        - 2 * ((-1) + 2 * nu) * r1_inv * d1 * d1 * dr12_inv * d2 * d2
          * dr13_inv * d3 - ((-3) + 4 * nu) * r1_inv * d2 * d2 * r1xy3_inv
        - (5 + 4 * nu * ((-3) + 2 * nu)) * r2xy2_inv * dp3
        - (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d2 * r2xy2_inv * dp3
        - (3 - 6 * nu + 4 * nu * nu) * r2_inv * d2 * d2 * r2xy3p_inv
        + 4 * (nu - 1) * ((-1) + 2 * nu) * dr12_inv * dr12_inv * d2 * d2 * y3
          * (2 * x3 + y3)
        - 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * dr12_inv * d2 * d2
          * y3 * (2 * x3 + y3)
        + 2 * ((-1) + 2 * nu) * dr12_inv * dr13p_inv
          * (((-1) + 2 * nu) * r2 * d1 * d1 * dp3
             - (nu - 1) * y3 * (2 * x3 + y3) * dr13p)
        - 4 * r2_inv * dr12_inv * dr12_inv * d2 * d2 * dr13p_inv
          * (x1 * x1 * x1 * x1 * y3 - 4 * x1 * x1 * x1 * y1 * y3
             - 3 * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             - 2 * x1 * y1 * y3
               * (d2 * d2 + 2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + y3 * (2 * x3 * x3 * x3 * x3 + 7 * x3 * x3 * x3 * y3
                     + (y1 * y1 + y3 * y3) * (y1 * y1 + y2 * y2 + y3 * y3)
                     + x3 * y3 * (6 * y1 * y1 + 3 * y2 * y2 + 5 * y3 * y3)
                     + x3 * x3 * (4 * y1 * y1 + 2 * y2 * y2 + 9 * y3 * y3)
                     + x2 * x2 * (y1 * y1 + dp3 * (2 * x3 + y3))
                     - 2 * x2 * y2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + x1 * x1 * y3
               * (d2 * d2 + 2 * (3 * y1 * y1 + dp3 * (2 * x3 + y3))))
        - 2 * dr12_inv * d2 * d2 * dr13p_inv * dr123p_32
          * (x1 * x1 * x1 * x1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             - 4 * x1 * x1 * x1 * y1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             - 3 * nu * (y1 * y1 + dp3 * dp3)
               * (x3 * x3 * x3 + 3 * x3 * x3 * y3
                  + y3 * (y1 * y1 + y2 * y2 - r2 * y3 + y3 * y3)
                  + x3 * (y1 * y1 + y2 * y2 - 2 * r2 * y3 + 3 * y3 * y3))
             + 2 * nu * nu * (y1 * y1 + dp3 * dp3)
               * (x3 * x3 * x3 + 3 * x3 * x3 * y3
                  + y3 * (y1 * y1 + y2 * y2 - r2 * y3 + y3 * y3)
                  + x3 * (y1 * y1 + y2 * y2 - 2 * r2 * y3 + 3 * y3 * y3))
             + y3 * (2 * x3 * x3 * x3 * x3 + 7 * x3 * x3 * x3 * y3
                     + (y1 * y1 + y3 * y3) * (y1 * y1 + y2 * y2 + y3 * y3)
                     + x3 * y3 * (6 * y1 * y1 + 3 * y2 * y2 + 5 * y3 * y3)
                     + x3 * x3 * (4 * y1 * y1 + 2 * y2 * y2 + 9 * y3 * y3)
                     - r2 * (2 * x3 + y3) * (y1 * y1 + dp3 * dp3))
             + 2 * x2 * y2 * (3 * nu * dp3 * (y1 * y1 + dp3 * dp3)
                              - 2 * nu * nu * dp3 * (y1 * y1 + dp3 * dp3)
                              - y3 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + x2 * x2 * ((-3) * nu * dp3 * (y1 * y1 + dp3 * dp3)
                          + 2 * nu * nu * dp3 * (y1 * y1 + dp3 * dp3)
                          + y3 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + 2 * x1 * y1
               * ((nu - 1) * ((-1) + 2 * nu) * r2 * y3 * (2 * x3 + y3)
                  + x2 * x2 * (-y3 + 3 * nu * dp3 - 2 * nu * nu * dp3)
                  + 2 * x2 * y2 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
                  + 3 * nu * dp3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  - 2 * nu * nu * dp3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  - y3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * (2 * x3 + y3)))
             + x1 * x1
               * (-(nu - 1) * ((-1) + 2 * nu) * r2 * y3 * (2 * x3 + y3)
                  + 2 * x2 * y2 * (-y3 + 3 * nu * dp3 - 2 * nu * nu * dp3)
                  + x2 * x2 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
                  - 3 * nu * dp3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  + 2 * nu * nu * dp3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  + y3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * (2 * x3 + y3))))
        + 2 * r2_inv * dr12_inv * dr13p_inv
          * (-x3 * y1 * y1 * y2 * y2 + 2 * x3 * x3 * x3 * x3 * y3
             + 4 * x3 * x3 * y1 * y1 * y3 + y1 * y1 * y1 * y1 * y3
             + 6 * x3 * x3 * y2 * y2 * y3 + 2 * y1 * y1 * y2 * y2 * y3
             + 7 * x3 * x3 * x3 * y3 * y3 + 6 * x3 * y1 * y1 * y3 * y3
             + 9 * x3 * y2 * y2 * y3 * y3 + 9 * x3 * x3 * y3 * y3 * y3
             + 2 * y1 * y1 * y3 * y3 * y3 + 3 * y2 * y2 * y3 * y3 * y3
             + 5 * x3 * y3 * y3 * y3 * y3 + y3 * y3 * y3 * y3 * y3
             - nu * dp3
               * (3 * x3 * x3 * x3 * x3 + 6 * x3 * x3 * y1 * y1
                  + 3 * y1 * y1 * y1 * y1 + 9 * x3 * x3 * y2 * y2
                  + 5 * y1 * y1 * y2 * y2
                  + 6 * x3 * (2 * (x3 * x3 + y1 * y1) + 3 * y2 * y2) * y3
                  + 3 * (6 * x3 * x3 + 2 * y1 * y1 + 3 * y2 * y2) * y3 * y3
                  + 12 * x3 * y3 * y3 * y3 + 3 * y3 * y3 * y3 * y3)
             + x1 * x1 * x1 * x1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             - 4 * x1 * x1 * x1 * y1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             + 2 * nu * nu * dp3
               * (x3 * x3 * x3 * x3 + y1 * y1 * y1 * y1 + 4 * x3 * x3 * x3 * y3
                  + 3 * y2 * y2 * y3 * y3 + y3 * y3 * y3 * y3
                  + y1 * y1 * (y2 * y2 + 2 * y3 * y3)
                  + x3 * x3 * (2 * y1 * y1 + 3 * y2 * y2 + 6 * y3 * y3)
                  + x3
                    * (4 * y1 * y1 * y3 + 6 * y2 * y2 * y3 + 4 * y3 * y3 * y3))
             + x2 * x2 * (-x3 * y1 * y1 + 6 * x3 * x3 * y3 + 2 * y1 * y1 * y3
                          + 9 * x3 * y3 * y3 + 3 * y3 * y3 * y3
                          + 2 * nu * nu * dp3 * (y1 * y1 + 3 * dp3 * dp3)
                          - nu * dp3 * (5 * y1 * y1 + 9 * dp3 * dp3))
             + 2 * x2 * y2
               * (x3 * y1 * y1 - 6 * x3 * x3 * y3 - 2 * y1 * y1 * y3
                  - 9 * x3 * y3 * y3 - 3 * y3 * y3 * y3
                  - 2 * nu * nu * dp3 * (y1 * y1 + 3 * dp3 * dp3)
                  + nu * dp3 * (5 * y1 * y1 + 9 * dp3 * dp3))
             + 2 * x1 * y1
               * (x2 * x2 * x3 - 2 * x2 * x3 * y2 + x3 * y2 * y2
                  - 2 * x2 * x2 * y3 - 4 * x3 * x3 * y3 - 2 * y1 * y1 * y3
                  + 4 * x2 * y2 * y3 - 2 * y2 * y2 * y3 - 6 * x3 * y3 * y3
                  - 2 * y3 * y3 * y3
                  - 2 * nu * nu * dp3 * (d2 * d2 + 2 * (y1 * y1 + dp3 * dp3))
                  + nu * dp3 * (5 * d2 * d2 + 6 * (y1 * y1 + dp3 * dp3)))
             + x1 * x1
               * ((-1) * x2 * x2 * x3 + 2 * x2 * x3 * y2 - x3 * y2 * y2
                  + 2 * x2 * x2 * y3 + 4 * x3 * x3 * y3 + 6 * y1 * y1 * y3
                  - 4 * x2 * y2 * y3 + 2 * y2 * y2 * y3 + 6 * x3 * y3 * y3
                  + 2 * y3 * y3 * y3
                  + 2 * nu * nu * dp3
                    * (d2 * d2 + 2 * (3 * y1 * y1 + dp3 * dp3))
                  - nu * dp3 * (5 * d2 * d2 + 6 * (3 * y1 * y1 + dp3 * dp3))))
        + xLogy (3 - 4 * nu, r1 + x3 - y3)
        + xLogy ((-3) + 6 * nu - 4 * nu * nu, r2 + x3 + y3);
    /// d/dz
    result (2)
      = 2 * ((-1) + 2 * nu) * r1 * d1 * d1 * dr12_inv * d2 * dr13_inv
        - ((-3) + 4 * nu) * r1_inv * r1xy2_inv * d3 * d3
        - 2 * ((-1) + 2 * nu) * r1_inv * d1 * d1 * dr12_inv * d2 * dr13_inv
          * d3 * d3 - ((-3) + 4 * nu) * d2 * r1xy3_inv
        - ((-3) + 4 * nu) * r1_inv * d2 * d3 * r1xy3_inv
        - 2 * r2_inv * x3 * dp3 / (r2 - x2 + y2)
        - (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * r2xy2_inv * dp3 * dp3
        - (3 - 6 * nu + 4 * nu * nu) * d2 * r2xy3p_inv
        - (3 - 6 * nu + 4 * nu * nu) * r2_inv * d2 * dp3 * r2xy3p_inv
        - 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * dr12_inv * d2 * y3
          * dp3 * (2 * x3 + y3)
        + 4 * (nu - 1) * ((-1) + 2 * nu) * dr12_inv * d2 * y3 * dp3
          * (2 * x3 + y3) * dr13p_inv
        + 2 * ((-1) + 2 * nu) * dr12_inv * d2 * dr13p_inv
          * (((-1) + 2 * nu) * r2 * d1 * d1
             - 2 * (nu - 1) * y3 * (d1 * d1 + dp3 * (3 * x3 + 2 * y3)))
        + 2 * r2_inv * dr12_inv * d2 * dr13p_inv
          * (nu * ((-3) + 2 * nu) * x1 * x1 * x1 * x1
             + 4 * (3 - 2 * nu) * nu * x1 * x1 * x1 * y1 - x3 * x3 * y1 * y1
             + 4 * x2 * x2 * x3 * y3 + 8 * x3 * x3 * x3 * y3
             + 6 * x3 * y1 * y1 * y3 - 8 * x2 * x3 * y2 * y3
             + 4 * x3 * y2 * y2 * y3 + 3 * x2 * x2 * y3 * y3
             + 21 * x3 * x3 * y3 * y3 + 5 * y1 * y1 * y3 * y3
             - 6 * x2 * y2 * y3 * y3 + 3 * y2 * y2 * y3 * y3
             + 18 * x3 * y3 * y3 * y3 + 5 * y3 * y3 * y3 * y3
             + 2 * x1 * y1 * ((1 + 2 * (7 - 4 * nu) * nu) * x3 * x3
                              + (3 - 2 * nu) * nu * (2 * y1 * y1 + d2 * d2)
                              - 2 * (3 + 2 * nu * ((-7) + 4 * nu)) * x3 * y3
                              + ((-5) + 2 * (7 - 4 * nu) * nu) * y3 * y3)
             + x1 * x1 * (((-1) + 2 * nu * ((-7) + 4 * nu)) * x3 * x3
                          + nu * ((-3) + 2 * nu) * (6 * y1 * y1 + d2 * d2)
                          + 2 * (3 + 2 * nu * ((-7) + 4 * nu)) * x3 * y3
                          + (5 + 2 * nu * ((-7) + 4 * nu)) * y3 * y3)
             - nu * (y1 * y1 + 3 * dp3 * dp3)
               * (3 * y1 * y1 + 3 * d2 * d2 + 5 * dp3 * dp3)
             + 2 * nu * nu
               * (5 * x3 * x3 * x3 * x3 + y1 * y1 * y1 * y1 + y1 * y1 * y2 * y2
                  + 20 * x3 * x3 * x3 * y3 + 4 * y1 * y1 * y3 * y3
                  + 3 * y2 * y2 * y3 * y3 + 5 * y3 * y3 * y3 * y3
                  + x3 * x3 * (4 * y1 * y1 + 3 * y2 * y2 + 30 * y3 * y3)
                  + x3
                    * (8 * y1 * y1 * y3 + 6 * y2 * y2 * y3 + 20 * y3 * y3 * y3)
                  + x2 * x2 * (y1 * y1 + 3 * dp3 * dp3)
                  - 2 * x2 * y2 * (y1 * y1 + 3 * dp3 * dp3)))
        - 4 * r2_inv * dr12_inv * d2 * dp3 * dr13p_inv * dr13p_inv
          * (x1 * x1 * x1 * x1 * y3 - 4 * x1 * x1 * x1 * y1 * y3
             - 3 * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * dr13p * (d1 * d1 + d2 * d2 + dp3 * dp3)
             - 2 * x1 * y1 * y3
               * (d2 * d2 + 2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + y3 * (2 * x3 * x3 * x3 * x3 + 7 * x3 * x3 * x3 * y3
                     + (y1 * y1 + y3 * y3) * (y1 * y1 + y2 * y2 + y3 * y3)
                     + x3 * y3 * (6 * y1 * y1 + 3 * y2 * y2 + 5 * y3 * y3)
                     + x3 * x3 * (4 * y1 * y1 + 2 * y2 * y2 + 9 * y3 * y3)
                     + x2 * x2 * (y1 * y1 + dp3 * (2 * x3 + y3))
                     - 2 * x2 * y2 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + x1 * x1 * y3
               * (d2 * d2 + 2 * (3 * y1 * y1 + dp3 * (2 * x3 + y3))))
        - 2 * dr12_inv * d2 * dp3 * dr13p_inv * dr123p_32
          * (x1 * x1 * x1 * x1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             - 4 * x1 * x1 * x1 * y1 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
             - 3 * nu * (y1 * y1 + dp3 * dp3)
               * (x3 * x3 * x3 + 3 * x3 * x3 * y3
                  + y3 * (y1 * y1 + y2 * y2 - r2 * y3 + y3 * y3)
                  + x3 * (y1 * y1 + y2 * y2 - 2 * r2 * y3 + 3 * y3 * y3))
             + 2 * nu * nu * (y1 * y1 + dp3 * dp3)
               * (x3 * x3 * x3 + 3 * x3 * x3 * y3
                  + y3 * (y1 * y1 + y2 * y2 - r2 * y3 + y3 * y3)
                  + x3 * (y1 * y1 + y2 * y2 - 2 * r2 * y3 + 3 * y3 * y3))
             + y3 * (2 * x3 * x3 * x3 * x3 + 7 * x3 * x3 * x3 * y3
                     + (y1 * y1 + y3 * y3) * (y1 * y1 + y2 * y2 + y3 * y3)
                     + x3 * y3 * (6 * y1 * y1 + 3 * y2 * y2 + 5 * y3 * y3)
                     + x3 * x3 * (4 * y1 * y1 + 2 * y2 * y2 + 9 * y3 * y3)
                     - r2 * (2 * x3 + y3) * (y1 * y1 + dp3 * dp3))
             + 2 * x2 * y2 * (3 * nu * dp3 * (y1 * y1 + dp3 * dp3)
                              - 2 * nu * nu * dp3 * (y1 * y1 + dp3 * dp3)
                              - y3 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + x2 * x2 * ((-3) * nu * dp3 * (y1 * y1 + dp3 * dp3)
                          + 2 * nu * nu * dp3 * (y1 * y1 + dp3 * dp3)
                          + y3 * (y1 * y1 + dp3 * (2 * x3 + y3)))
             + 2 * x1 * y1
               * ((nu - 1) * ((-1) + 2 * nu) * r2 * y3 * (2 * x3 + y3)
                  + x2 * x2 * (-y3 + 3 * nu * dp3 - 2 * nu * nu * dp3)
                  + 2 * x2 * y2 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
                  + 3 * nu * dp3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  - 2 * nu * nu * dp3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  - y3 * (2 * y1 * y1 + y2 * y2 + 2 * dp3 * (2 * x3 + y3)))
             + x1 * x1
               * (-(nu - 1) * ((-1) + 2 * nu) * r2 * y3 * (2 * x3 + y3)
                  + 2 * x2 * y2 * (-y3 + 3 * nu * dp3 - 2 * nu * nu * dp3)
                  + x2 * x2 * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
                  - 3 * nu * dp3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  + 2 * nu * nu * dp3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * dp3)
                  + y3 * (6 * y1 * y1 + y2 * y2 + 2 * dp3 * (2 * x3 + y3))))
        + xLogy ((-2), r2 - x2 + y2) + xLogy (3 - 4 * nu, r1 + x2 - y2)
        + xLogy ((-5) + 4 * (3 - 2 * nu) * nu, r2 + x2 - y2);
    return result;
  }
}
