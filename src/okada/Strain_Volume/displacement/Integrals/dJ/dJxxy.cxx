#include "../../Integrals.hxx"

namespace okada
{
  FTensor::Tensor1<double, 3> Integrals::dJxxy (const int &ix) const
  {
    const int iy (1 - ix);
    const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2), d3 (x3 - y3),
      dp3 (x3 + y3), dr12_inv (1 / (d1 * d1 + d2 * d2)),
      dr23_inv (1 / (d2 * d2 + d3 * d3)),
      dr23p (d2 * d2 + dp3 * dp3), dr23p_inv (1 / dr23p),
      r1xy1_inv (1 / (r1 + x1 - y1)),
      r1xy3_inv (1 / (r1 + x3 - y3)), r2xy1_inv (1 / (r2 + x1 - y1)),
      r2xy3p_inv (1 / (r2 + x3 + y3)),
      r1_inv (1 / r1), r2_inv (1 / r2),
      dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5)),
      x2d1 (1 / (x2 * x2 + d1 * d1)),
      nx2d1 (1 / (9 * x2 * x2 + d1 * d1)), 
      nux2d1 (1 / (nu * nu * x2 * x2 + d1 * d1)),
      y2d1 (1 / (y2 * y2 + d1 * d1)),
      ny2d1 (1 / (9 * y2 * y2 + d1 * d1)), 
      nuy2d1 (1 / (nu * nu * y2 * y2 + d1 * d1));

    FTensor::Tensor1<double, 3> result;
    /// d/dx
    result (ix)
      = x2 * x2 * x2d1 + 9 * x2 * x2 * nx2d1 + 4 * nu * nu * x2 * x2 * nux2d1
        + y2 * y2 * y2d1 + 9 * y2 * y2 * ny2d1 + 4 * nu * nu * y2 * y2 * nuy2d1
        - 4 * (nu - 1) * r1xy1_inv * d3
        - 4 * (nu - 1) * r1_inv * d1 * r1xy1_inv * d3
        + 4 * (nu - 1) * r1 * dr12_inv * d2 * d2 * dr23_inv * d3
        - 4 * (nu - 1) * r1_inv * d1 * d1 * dr12_inv * d2 * d2 * dr23_inv * d3
        - ((-3) + 4 * nu) * r1_inv * d1 * d1 * r1xy3_inv
        + 4 * (nu - 1) * r2xy1_inv * dp3
        + 4 * (nu - 1) * r2_inv * d1 * r2xy1_inv * dp3
        - (3 - 6 * nu + 4 * nu * nu) * r2_inv * d1 * d1 * r2xy3p_inv
        - 4 * (nu - 1) * ((-1) + 2 * nu) * d1 * d1 * dr12_inv * dr12_inv * y3
          * (2 * x3 + y3)
        + 2 * (nu - 1) * ((-1) + 2 * nu) * dr12_inv * y3 * (2 * x3 + y3)
        + 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * d1 * d1 * dr12_inv
          * y3 * (2 * x3 + y3)
        - 4 * (nu - 1) * r2 * dr12_inv * d2 * d2 * dp3 * dr23p_inv
        + 4 * (nu - 1) * r2_inv * d1 * d1 * dr12_inv * d2 * d2 * dp3
          * dr23p_inv
        + 2 * d1 * d1 * dr12_inv * dr123p_32
          * ((-3) * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                          + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                            * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + 2 * nu * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                              + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                                * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + y3 * (d1 * d1 + d2 * d2 - (r2 - x3 - y3) * (2 * x3 + y3)))
        + 4 * r2_inv * d1 * d1 * dr12_inv * dr12_inv
          * ((-3) * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + y3 * (d1 * d1 + d2 * d2 + dp3 * (2 * x3 + y3)))
        + r2_inv * dr12_inv
          * (6 * nu * dp3 * (3 * d1 * d1 + d2 * d2 + dp3 * dp3)
             - 4 * nu * nu * dp3 * (3 * d1 * d1 + d2 * d2 + dp3 * dp3)
             - 2 * y3 * (3 * d1 * d1 + d2 * d2 + dp3 * (2 * x3 + y3)))
        + xLogy (3 - 4 * nu, r1 + x3 - y3)
        + xLogy ((-3) + 6 * nu - 4 * nu * nu, r2 + x3 + y3);
    /// d/dy
    result (iy)
      = x2 * x2d1 * (-x1 + y1) + 9 * x2 * nx2d1 * (-x1 + y1)
        + 4 * nu * nu * x2 * nux2d1 * (-x1 + y1)
        - 4 * (nu - 1) * r1_inv * r1xy1_inv * d2 * d3
        - 4 * (nu - 1) * r1 * d1 * dr12_inv * d2 * dr23_inv * d3
        + (-4) * (nu - 1) * r1_inv * d1 * dr12_inv * d2 * d2 * d2 * dr23_inv
          * d3 - ((-3) + 4 * nu) * r1_inv * d1 * d2 * r1xy3_inv
        + 4 * (nu - 1) * r2_inv * r2xy1_inv * d2 * dp3
        - (3 + (-6) * nu + 4 * nu * nu) * r2_inv * d1 * d2 * r2xy3p_inv
        - 4 * (nu - 1) * ((-1) + 2 * nu) * d1 * dr12_inv * dr12_inv * d2 * y3
          * (2 * x3 + y3)
        + 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * d1 * dr12_inv * d2
          * y3 * (2 * x3 + y3)
        - 4 * r2_inv * d1 * dr12_inv * d2
          * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
        + 4 * (nu - 1) * r2 * d1 * dr12_inv * d2 * dp3 * dr23p_inv
        + 4 * (nu - 1) * r2_inv * d1 * dr12_inv * d2 * d2 * d2 * dp3
          * dr23p_inv
        + 2 * d1 * dr12_inv * d2 * dr123p_32
          * ((-3) * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                          + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                            * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + 2 * nu * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                              + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                                * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + y3 * (d1 * d1 + d2 * d2 - (r2 - x3 - y3) * (2 * x3 + y3)))
        + 4 * r2_inv * d1 * dr12_inv * dr12_inv * d2
          * ((-3) * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + y3 * (d1 * d1 + d2 * d2 + dp3 * (2 * x3 + y3)))
        + atan2 (x1 - y1, (-1) * x2) - 3 * atan2 (3 * x2, x1 - y1)
        + 4 * nu * atan2 (-nu * x2, x1 - y1)
        + (4 - 4 * nu) * atan2 (r1 * d2, d1 * d3)
        + 4 * (nu - 1) * atan2 (r2 * d2, d1 * dp3);
    /// d/dz
    result (2)
      = 4 * (nu - 1) * r1 * d1 * dr12_inv * d2 * d2 * dr23_inv
        - 4 * (nu - 1) * r1_inv * r1xy1_inv * d3 * d3
        - 4 * (nu - 1) * r1_inv * d1 * dr12_inv * d2 * d2 * dr23_inv * d3 * d3
        - ((-3) + 4 * nu) * d1 * r1xy3_inv
        - ((-3) + 4 * nu) * r1_inv * d1 * d3 * r1xy3_inv
        + 4 * (nu - 1) * ((-1) + 2 * nu) * d1 * dr12_inv * y3
        + 4 * (nu - 1) * r2_inv * r2xy1_inv * dp3 * dp3
        - (3 + (-6) * nu + 4 * nu * nu) * d1 * r2xy3p_inv
        - (3 - 6 * nu + 4 * nu * nu) * r2_inv * d1 * dp3 * r2xy3p_inv
        + 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * d1 * dr12_inv * y3
          * dp3 * (2 * x3 + y3)
        - 2 * r2_inv * d1 * dr12_inv
          * (3 * nu * ((-3) + 2 * nu) * x3 * x3
             + nu * ((-3) + 2 * nu) * (d1 * d1 + d2 * d2)
             + 2 * (2 - 9 * nu + 6 * nu * nu) * x3 * y3
             + (3 - 9 * nu + 6 * nu * nu) * y3 * y3)
        - 4 * (nu - 1) * r2 * d1 * dr12_inv * d2 * d2 * dr23p_inv
        + 4 * (nu - 1) * r2_inv * d1 * dr12_inv * d2 * d2 * dp3 * dp3
          * dr23p_inv
        + 2 * d1 * dr12_inv * dp3 * dr123p_32
          * ((-3) * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                          + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                            * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + 2 * nu * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                              + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                                * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + y3 * (d1 * d1 + d2 * d2 - (r2 - x3 - y3) * (2 * x3 + y3)))
        + xLogy (4 - 4 * nu, r1 + x1 - y1)
        + xLogy (4 * (nu - 1), r2 + x1 - y1);

    return result;
  }
}
