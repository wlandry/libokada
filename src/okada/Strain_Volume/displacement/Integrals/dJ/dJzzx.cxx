#include "../../Integrals.hxx"

namespace okada
{
  FTensor::Tensor1<double, 3> Integrals::dJzzx (const int &ix) const
  {
    const int iy (1 - ix);
    const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2), d3 (x3 - y3),
      dp3 (x3 + y3), dr12_inv (1 / (d1 * d1 + d2 * d2)),
      dr13_inv (1 / (d1 * d1 + d3 * d3)),
      dr13p (d1 * d1 + dp3 * dp3),
      dr13p_inv (1 / dr13p), 
      r1xy2_inv (1 / (r1 + x2 - y2)),
      r1xy3_inv (1 / (r1 + x3 - y3)), 
      r2xy2_inv (1 / (r2 + x2 - y2)), 
      r2xy3p_inv (1 / (r2 + x3 + y3)),
      r1_inv (1 / r1), r2_inv (1 / r2),
      dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5));
      
    FTensor::Tensor1<double, 3> result;
    /// d/dx
    result (ix)
      = 4 * r2_inv * x3 * ((-1) * x1 + y1) / (r2 - x2 + y2)
      - ((-3) + 4 * nu) * r1_inv * d1 * r1xy2_inv * d3
      - 4 * (nu - 1) * r1 * d1 * dr12_inv * d2 * dr13_inv * d3
      - 4 * (nu - 1) * r1_inv * d1 * d1 * d1 * dr12_inv * d2 * dr13_inv * d3
      - 4 * (nu - 1) * r1_inv * d1 * d2 * r1xy3_inv
      - 8 * (nu - 1) * (nu - 1) * r2_inv * d1 * d2 * r2xy3p_inv
      - r2_inv * d1 * r2xy2_inv
      * (7 * x3 + 5 * y3 - 12 * nu * dp3 + 8 * nu * nu * dp3)
      - 4 * r2_inv * x3 * d1 * d2 * y3 * dp3 * dr13p_inv * dr13p_inv
      - 8 * (nu - 1) * (nu - 1) * r2 * d1 * dr12_inv * d2 * dp3 * dr13p_inv
      - 8 * (nu - 1) * (nu - 1) * r2_inv * d1 * d1 * d1 * dr12_inv * d2 * dp3
      * dr13p_inv - 2 * x3 * d1 * d2 * y3 * dp3 * dr13p_inv * dr123p_32
      + 5 * atan2 ((-5) * x1, x2 - y2) - 3 * atan2 (3 * x1, x2 - y2)
      + 4 * nu * atan2 (-nu * x1, x2 - y2)
      - 12 * nu * atan2 (3 * nu * x1, x2 - y2)
      + 8 * nu * nu * atan2 (-nu * nu * x1, x2 - y2)
      + (4 - 4 * nu) * atan2 (r1 * d1, d2 * d3)
      - 8 * (nu - 1) * (nu - 1) * atan2 (r2 * d1, d2 * dp3);
    /// d/dy
    result (iy)
      = 4 * x3 / (r2 - x2 + y2)
      + 4 * r2_inv * x3 * (-x2 + y2) / (r2 - x2 + y2)
      - ((-3) + 4 * nu) * r1xy2_inv * d3
      - ((-3) + 4 * nu) * r1_inv * d2 * r1xy2_inv * d3
      + 4 * (nu - 1) * r1 * d1 * d1 * dr12_inv * dr13_inv * d3
      - 4 * (nu - 1) * r1_inv * d1 * d1 * dr12_inv * d2 * d2 * dr13_inv * d3
      - 4 * (nu - 1) * r1_inv * d2 * d2 * r1xy3_inv
      - 8 * (nu - 1) * (nu - 1) * r2_inv * d2 * d2 * r2xy3p_inv
      + r2xy2_inv * ((-7) * x3 - 5 * y3 + 12 * nu * dp3 - 8 * nu * nu * dp3)
      - r2_inv * d2 * r2xy2_inv
      * (7 * x3 + 5 * y3 - 12 * nu * dp3 + 8 * nu * nu * dp3)
      + 8 * (nu - 1) * (nu - 1) * r2 * d1 * d1 * dr12_inv * dp3 * dr13p_inv
      - 8 * (nu - 1) * (nu - 1) * r2_inv * d1 * d1 * dr12_inv * d2 * d2 * dp3
      * dr13p_inv + 2 * r2_inv * x3 * y3 * dp3 * dr13p_inv
      - 2 * x3 * d2 * d2 * y3 * dp3 * dr13p_inv * dr123p_32
      + xLogy (4 - 4 * nu, r1 + x3 - y3)
      + xLogy ((-8) * (nu - 1) * (nu - 1), r2 + x3 + y3);
    /// d/dz
    result (2)
      = 4 * (nu - 1) * r1 * d1 * d1 * dr12_inv * d2 * dr13_inv
      - ((-3) + 4 * nu) * r1_inv * r1xy2_inv * d3 * d3
      - 4 * (nu - 1) * r1_inv * d1 * d1 * dr12_inv * d2 * dr13_inv * d3 * d3
      - 4 * (nu - 1) * d2 * r1xy3_inv
      - 4 * (nu - 1) * r1_inv * d2 * d3 * r1xy3_inv
      - 4 * r2_inv * x3 * dp3 / (r2 - x2 + y2)
      - 8 * (nu - 1) * (nu - 1) * d2 * r2xy3p_inv
      - 8 * (nu - 1) * (nu - 1) * r2_inv * d2 * dp3 * r2xy3p_inv
      - r2_inv * r2xy2_inv * dp3
      * (7 * x3 + 5 * y3 - 12 * nu * dp3 + 8 * nu * nu * dp3)
      - 4 * r2_inv * x3 * d2 * y3 * dp3 * dp3 * dr13p_inv * dr13p_inv
      + 8 * (nu - 1) * (nu - 1) * r2 * d1 * d1 * dr12_inv * d2 * dr13p_inv
      - 8 * (nu - 1) * (nu - 1) * r2_inv * d1 * d1 * dr12_inv * d2 * dp3
      * dp3 * dr13p_inv + 2 * r2_inv * d2 * y3 * (2 * x3 + y3) * dr13p_inv
      - 2 * x3 * d2 * y3 * dp3 * dp3 * dr13p_inv * dr123p_32
      + xLogy ((-4), r2 - x2 + y2) + xLogy (3 - 4 * nu, r1 + x2 - y2)
      + xLogy ((-7) + 4 * (3 - 2 * nu) * nu, r2 + x2 - y2);
    return result;
  }
}
