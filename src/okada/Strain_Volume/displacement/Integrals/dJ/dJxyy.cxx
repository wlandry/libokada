#include "../../Integrals.hxx"

namespace okada
{
  FTensor::Tensor1<double, 3> Integrals::dJxyy (const int &ix) const
  {
    const int iy (1 - ix);
    const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2), d3 (x3 - y3),
      dp3 (x3 + y3), dr12_inv (1 / (d1 * d1 + d2 * d2)),
      r1xy3_inv (1 / (r1 + x3 - y3)),
      r2xy3p_inv (1 / (r2 + x3 + y3)),
      r1_inv (1 / r1), r2_inv (1 / r2),
      dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5));

    FTensor::Tensor1<double, 3> result;
    /// d/dx
    result (ix)
      = -r1_inv * d1 * d2 * r1xy3_inv
        - ((-1) - 2 * nu + 4 * nu * nu) * r2_inv * d1 * d2 * r2xy3p_inv
        - 4 * (nu - 1) * ((-1) + 2 * nu) * d1 * dr12_inv * dr12_inv * d2 * y3
          * (2 * x3 + y3)
        + 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * d1 * dr12_inv * d2
          * y3 * (2 * x3 + y3)
        - 4 * r2_inv * d1 * dr12_inv * d2
          * (y3 - 3 * nu * dp3 + 2 * nu * nu * dp3)
        + 2 * d1 * dr12_inv * d2 * dr123p_32
          * ((-3) * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                          + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                            * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + 2 * nu * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                              + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                                * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + y3 * (d1 * d1 + d2 * d2 - (r2 - x3 - y3) * (2 * x3 + y3)))
        + 4 * r2_inv * d1 * dr12_inv * dr12_inv * d2
          * ((-3) * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + y3 * (d1 * d1 + d2 * d2 + dp3 * (2 * x3 + y3)));
  /// d/dy
  result (iy)
      = -r1_inv * d2 * d2 * r1xy3_inv
        - ((-1) - 2 * nu + 4 * nu * nu) * r2_inv * d2 * d2 * r2xy3p_inv
        + 2 * (nu - 1) * ((-1) + 2 * nu) * dr12_inv * y3 * (2 * x3 + y3)
        - 4 * (nu - 1) * ((-1) + 2 * nu) * dr12_inv * dr12_inv * d2 * d2 * y3
          * (2 * x3 + y3)
        + 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * dr12_inv * d2 * d2
          * y3 * (2 * x3 + y3)
        + 2 * dr12_inv * d2 * d2 * dr123p_32
          * ((-3) * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                          + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                            * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + 2 * nu * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                              + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                                * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + y3 * (d1 * d1 + d2 * d2 - (r2 - x3 - y3) * (2 * x3 + y3)))
        + 4 * r2_inv * dr12_inv * dr12_inv * d2 * d2
          * ((-3) * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + 2 * nu * nu * dp3 * (d1 * d1 + d2 * d2 + dp3 * dp3)
             + y3 * (d1 * d1 + d2 * d2 + dp3 * (2 * x3 + y3)))
        + r2_inv * dr12_inv
          * (6 * nu * dp3 * (d1 * d1 + 3 * d2 * d2 + dp3 * dp3)
             - 4 * nu * nu * dp3 * (d1 * d1 + 3 * d2 * d2 + dp3 * dp3)
             - 2 * y3 * (d1 * d1 + 3 * d2 * d2 + dp3 * (2 * x3 + y3)))
        + log (r2 + x3 + y3) + xLogy ((-1), r1 + x3 - y3)
        + xLogy (2 * (1 - 2 * nu) * nu, r2 + x3 + y3);
  /// d/dz
  result (2)
      = (-x2 + y2) * r1xy3_inv - r1_inv * d2 * d3 * r1xy3_inv
        + 4 * (nu - 1) * ((-1) + 2 * nu) * dr12_inv * d2 * y3
        - ((-1) - 2 * nu + 4 * nu * nu) * d2 * r2xy3p_inv
        - ((-1) - 2 * nu + 4 * nu * nu) * r2_inv * d2 * dp3 * r2xy3p_inv
        + 2 * (nu - 1) * ((-1) + 2 * nu) * (1 / (r2 * r2)) * dr12_inv * d2 * y3
          * dp3 * (2 * x3 + y3)
        - 2 * r2_inv * dr12_inv * d2
          * (3 * nu * ((-3) + 2 * nu) * x3 * x3
             + nu * ((-3) + 2 * nu) * (d1 * d1 + d2 * d2)
             + 2 * (2 - 9 * nu + 6 * nu * nu) * x3 * y3
             + (3 - 9 * nu + 6 * nu * nu) * y3 * y3)
        + 2 * dr12_inv * d2 * dp3 * dr123p_32
          * ((-3) * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                          + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                            * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + 2 * nu * nu * (x3 * (x3 * x3 + d1 * d1 + d2 * d2)
                              + (x3 * ((-2) * r2 + 3 * x3) + d1 * d1 + d2 * d2)
                                * y3 - (r2 - 3 * x3) * y3 * y3 + y3 * y3 * y3)
             + y3 * (d1 * d1 + d2 * d2 - (r2 - x3 - y3) * (2 * x3 + y3)));
  return result;
}
}
