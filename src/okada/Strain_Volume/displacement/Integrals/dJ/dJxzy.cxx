#include "../../Integrals.hxx"

namespace okada
{
  FTensor::Tensor1<double, 3> Integrals::dJxzy (const int &ix) const
  {
    const int iy (1 - ix);
    const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2), d3 (x3 - y3),
      dp3 (x3 + y3), dr12_inv (1 / (d1 * d1 + d2 * d2)),
      dr23p (d2 * d2 + dp3 * dp3), r2xy3p_inv (1 / (r2 + x3 + y3)),
      r1_inv (1 / r1), r2_inv (1 / r2),
      dr123p_12 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -0.5)),
      dr123p_22 (1 / (d1 * d1 + d2 * d2 + dp3 * dp3)),
      dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5));

    FTensor::Tensor1<double, 3> result;
    /// d/dx
    result (ix) = -(r1_inv * d1 + 2 * (7 + 8 * ((-2) + nu) * nu) * r2_inv * d1
                    - 2 * r2_inv * d1 * r2xy3p_inv
                    * (3 * x3 + 2 * y3 - 6 * nu * dp3 + 4 * nu * nu * dp3)
                    - 2 * ((-3) + 4 * nu) * x3 * d1 * dr12_inv * dp3 * dr123p_12
                    + d1 * dr123p_32
                    * (((-7) - 8 * ((-2) + nu) * nu) * x1 * x1
                       + 2 * (7 + 8 * ((-2) + nu) * nu) * x1 * y1
                       + ((-7) - 8 * ((-2) + nu) * nu) * y1 * y1
                       - 7 * (x3 * x3 + d2 * d2) - 16 * x3 * y3 - 7 * y3 * y3
                       + (-8) * ((-2) + nu) * nu * dr23p));
    /// d/dy
    result (iy) = -(r1_inv * d2 + 2 * (7 + 8 * ((-2) + nu) * nu) * r2_inv * d2
                    - 2 * r2_inv * d2 * r2xy3p_inv
                    * (3 * x3 + 2 * y3 - 6 * nu * dp3 + 4 * nu * nu * dp3)
                    - 2 * ((-3) + 4 * nu) * x3 * dr12_inv * d2 * dp3 * dr123p_12
                    + d2 * dr123p_32
                    * (((-7) - 8 * ((-2) + nu) * nu) * x1 * x1
                       + 2 * (7 + 8 * ((-2) + nu) * nu) * x1 * y1
                       + ((-7) - 8 * ((-2) + nu) * nu) * y1 * y1
                       - 7 * (x3 * x3 + d2 * d2) - 16 * x3 * y3 - 7 * y3 * y3
                       + (-8) * ((-2) + nu) * nu * dr23p));
    /// d/dz
    result (2) = -(r1_inv * d3
                   + 2 * r2_inv * ((7 + 8 * ((-2) + nu) * nu) * x3
                                   + 8 * (nu - 1) * (nu - 1) * y3)
                   + 2 * r2xy3p_inv
                   * ((-3) * x3 - 2 * y3 + 6 * nu * dp3 - 4 * nu * nu * dp3)
                   - 2 * r2_inv * dp3 * r2xy3p_inv
                   * (3 * x3 + 2 * y3 - 6 * nu * dp3 + 4 * nu * nu * dp3)
                   - 2 * ((-3) + 4 * nu) * x3 * dr12_inv * dp3 * dp3 * dr123p_12
                   + dp3 * dr123p_32
                   * (((-7) - 8 * ((-2) + nu) * nu) * x1 * x1
                      + 2 * (7 + 8 * ((-2) + nu) * nu) * x1 * y1
                      + ((-7) - 8 * ((-2) + nu) * nu) * y1 * y1
                      - 7 * (x3 * x3 + d2 * d2) - 16 * x3 * y3 + (-7) * y3 * y3
                      - 8 * ((-2) + nu) * nu * dr23p)
                   + 2 * ((-3) + 4 * nu) * r2_inv * x3
                   / (1 - dp3 * dp3 * dr123p_22)
                   + 2 * ((-3) + 4 * nu) * acoth (r2_inv * dp3)
                   + xLogy ((-6) + 4 * (3 - 2 * nu) * nu, r2 + x3 + y3));
    return result;
  }
}
