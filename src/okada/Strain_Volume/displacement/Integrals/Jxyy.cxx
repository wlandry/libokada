#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jxyy(const int &ix) const
  {
    const int iy(1-ix);
    const double x1(xyz(ix)), x2(xyz(iy)), x3(xyz(2)), y1(y(ix)), y2(y(iy)), y3(y(2));
      
    return 2*(1/r2)*(1/((x1-y1)*(x1-y1)+(x2-y2)*(x2-y2)))
      * (x2-y2)*(-(nu-1)*((-1)+2*nu)*r2*r2*(x3+y3)
                 + (nu-1)*((-1)+2*nu)*r2*y3*(2*x3+y3)
                 + x3*((x1-y1)*(x1-y1)+(x2-y2)*(x2-y2)+x3*(x3+y3)))
      + xLogy(-((-1)-2*nu+4*nu*nu)*(x2-y2),r2+x3+y3)
      + xLogy(-x2+y2,r1+x3-y3);
  }
}
