#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jxxy(const int &ix) const
  {
    const int iy(1-ix);
    const double x1(xyz(ix)), x2(xyz(iy)), x3(xyz(2)), y1(y(ix)), y2(y(iy)), y3(y(2));
      
    return 2 * (1/r2) * (x1-y1) * (1/((x1-y1)*(x1-y1)+(x2-y2)*(x2-y2)))
      * (-(nu-1)*((-1)+2*nu) * r2*r2 * (x3+y3) + (nu-1) * ((-1)+2*nu) * r2*y3 * (2*x3+y3)
         + x3*((x1-y1)*(x1-y1)+(x2-y2)*(x2-y2)+x3*(x3+y3)))
      + x2*atan2(-x2,x1-y1)
      - 3*x2*atan2(3*x2,x1-y1)
      + 4*nu*x2*atan2(-nu*x2,x1-y1)
      - 4*(nu-1)*(x2-y2)*atan2(r1*(x2-y2),(x1-y1)*(x3-y3))
      + 4*(nu-1)*(x2-y2)*atan2(r2*(x2-y2),(x1-y1)*(x3+y3))
      + 3*y2*atan2((-3)*y2,x1-y1)-y2*atan2(y2,x1-y1)
      - 4*nu*y2*atan2(nu*y2,x1-y1)
      + xLogy((-1)*((-3)+4*nu)*(x1-y1),r1+x3-y3)
      + xLogy(-(3-6*nu+4*nu*nu)*(x1-y1),r2+x3+y3)
      + xLogy((-4)*(nu-1)*(x3-y3),r1+x1-y1)
      + xLogy(4*(nu-1)*(x3+y3),r2+x1-y1);
  }
}
