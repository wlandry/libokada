#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jxxx(const int &ix) const
  {
    const int iy(1-ix);
    const double x1(xyz(ix)), x2(xyz(iy)), x3(xyz(2)), y1(y(ix)), y2(y(iy)), y3(y(2));

    return (-2)*(1/r2)*(1/((x1-y1)*(x1-y1)+(x2-y2)*(x2-y2)))*(x2-y2)
      * (1/((x1-y1)*(x1-y1)+(x3+y3)*(x3+y3)))
      * (x3*((x3*x3 + (x1-y1)*(x1-y1))*(x3*x3 + (x1-y1)*(x1-y1) + (x2-y2)*(x2-y2))
             + x3*(3*x3*x3 + 2*(x1-y1)*(x1-y1) + (x2-y2)*(x2-y2))*y3
             + 3*x3*x3*y3*y3+x3*y3*y3*y3)
         - (nu-1)*((-1)+2*nu)*r2*r2*(x3+y3)*((x1-y1)*(x1-y1)+(x3+y3)*(x3+y3))
         + (nu-1)*((-1)+2*nu)*r2*y3*(2*x3+y3)*((x1-y1)*(x1-y1)+(x3+y3)*(x3+y3)))
      + 2*(nu-1)*((-1)+2*nu)*(x1-y1)*atan3((x1-y1),(x2-y2))
      + x1*atan2(-x1,x2-y2)
      - 3*x1*atan2(3*x1,x2-y2)
      + 4*nu*x1*atan2(-nu*x1,x2-y2)
      + 3*y1*atan2((-3)*y1,x2-y2)
      - y1*atan2(y1,x2-y2)
      - 4*nu*y1*atan2(nu*y1,x2-y2)
      + 2*((-1)+2*nu)*(x1-y1)*atan2(r1*(-x1+y1),(x2-y2)*(x3+(-1)*y3))
      + 2*(1-2*nu)*(1-2*nu)*(x1-y1)*atan2(r2*(-x1+y1),(x2-y2)*(x3+y3))
      + xLogy((-2)*x3,r2-x2+y2)
      + xLogy((-1)*((-3)+4*nu)*(x2-y2),r1+x3-y3)
      + xLogy(-(3+(-6)*nu+4*nu*nu)*(x2-y2),r2+x3+y3)
      + xLogy(-((-3)+4*nu)*(x3-y3),r1+x2-y2)
      + xLogy(-(5+4*nu*((-3)+2*nu))*(x3+y3),r2+x2-y2);
  }
}
