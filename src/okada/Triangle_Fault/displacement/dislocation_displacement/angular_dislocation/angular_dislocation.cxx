/// Transforms coordinates of the calculation points as well as slip
/// vector components from ADCS into TDCS. It then calculates the
/// displacements in ADCS and transforms them into TDCS.

#include "../../../../Displacement.hxx"
/// For M_PI
#define _USE_MATH_DEFINES
#include <cmath>

namespace okada
{
  Displacement
  angular_dislocation_unrotated(const FTensor::Tensor1<double,3> &xyz,
                                const double &alpha,
                                const FTensor::Tensor1<double,3> &b1,
                                const double &nu);

  Displacement
  angular_dislocation(const FTensor::Tensor1<double,3> &xyz,
                      const double &alpha,
                      const FTensor::Tensor1<double,3> &jump_vector,
                      const double &nu,
                      const FTensor::Tensor1<double,3> &TriVertex,
                      const FTensor::Tensor1<double,3> &SideVec,
                      const double &sign)
  {
    /// Transformation matrix from TDCS into ADCS
    FTensor::Tensor2<double,3,3> A(1,0,0,
                                   0,sign*SideVec(2),-sign*SideVec(1),
                                   0,sign*SideVec(1), sign*SideVec(2));
    FTensor::Tensor1<double,3> xyz1;
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Index<'l',3> l;
    xyz1(i)=A(i,j)*(xyz(j)-TriVertex(j));
    /// FIXME: I do not understand why the tensile slip Trivertex(0) is
    /// neglected here
    xyz1(0)=xyz(0);
  
    FTensor::Tensor1<double,3> b1;
    b1(i) = A(i,j)*jump_vector(j);

    /// Calculate displacements associated with an angular dislocation
    /// in ADCS and transform back into TDCS
    Displacement unrotated(angular_dislocation_unrotated(xyz1,-M_PI+alpha,
                                                         b1,nu)), result;
    result.first(i)=A(j,i)*unrotated.first(j);
    result.second(i,j)=(A(k,j)*unrotated.second(k,l))^A(l,i);

    return result;
  }
}
