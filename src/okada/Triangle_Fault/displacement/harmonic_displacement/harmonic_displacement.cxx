#include "../../../Displacement.hxx"

namespace okada
{
  Displacement
  free_surface_correction(const FTensor::Tensor1<double,3> &coord,
                          const FTensor::Tensor1<double,3> &jump,
                          const FTensor::Tensor1<double,3> &PA,
                          const FTensor::Tensor1<double,3> &PB,
                          const double &nu);

  Displacement
  harmonic_displacement
  (const FTensor::Tensor1<double,3> &coord,
   const FTensor::Tensor1<double,3> &jump,
   const FTensor::Tensor1<double,3> &origin,
   const FTensor::Tensor1<double,3> &u,
   const FTensor::Tensor1<double,3> &v,
   const double &nu)
  {
    FTensor::Tensor1<double,3> P1, P2, P3;
    Displacement result;
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;

    P2(i)=origin(i)+u(i);
    P3(i)=origin(i)+v(i);
    /// Calculate contribution of angular dislocation pair on each TD side
    Displacement p12(free_surface_correction(coord,jump,origin,P2,nu)),
      p23(free_surface_correction(coord,jump,P2,P3,nu)),
      p31(free_surface_correction(coord,jump,P3,origin,nu));

    result.first(i)=p12.first(i)+p23.first(i)+p31.first(i);
    result.second(i,j)=p12.second(i,j)+p23.second(i,j)+p31.second(i,j);
    return result;
  }
}
