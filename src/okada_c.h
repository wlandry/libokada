#pragma once

/* origin, coord, and displacement are of length 3.  Strain is of
   length 6.  The results are placed in displacement and strain.
   Strain represents the symmetric strain tensor, with the mapping
   from the vector to the ordering being (xx, xy, xz, yy, yz, zz). */


void okada_displacement (double lambda, double mu, double slip, double opening,
                         double width, double length, double strike,
                         double dip, double rake, double origin[],
                         double coord[],
                         double *displacement,
                         double *strain);

/* As above with u, v and jump being of length 3. */

void triangle_displacement (double origin[], double u[], double v[],
                            double jump[], double lamdba, double mu,
                            double coord[],
                            double *displacement,
                            double *strain);

/* As above, with applied_strain and output_strain being of length 6 */

void strain_volume_displacement (double origin[], double shape[], double strike,
                                 double applied_strain[],
                                 double lamdba, double mu,
                                 double coord[],
                                 double *displacement,
                                 double *output_strain);
