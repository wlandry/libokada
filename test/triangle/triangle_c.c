#include "../../src/okada_c.h"

#include <stdio.h>


int main(int argc, char *argv[])
{
  double lambda=1.0, mu=1.0;
  double origin[3], u[3], v[3], jump[3], coord[3], displacement[3], strain[6];
  origin[0]=3.14;
  origin[1]=1.5;
  origin[2]=9.2;
  u[0]=6.5;
  u[1]=3.5;
  u[2]=9.2;
  v[0]=-7.1;
  v[1]=8.2;
  v[2]=8.1;
  jump[0]=8.2;
  jump[1]=-4.6;
  jump[2]=-2.2;
  coord[0]=9.5;
  coord[1]=-8.5;
  coord[2]=8.5;
  triangle_displacement(origin,u,v,jump,lambda,mu,coord,displacement,strain);

  printf("Matlab Values\n");
  printf("[-0.0380368, 0.178467, -0.00221999]\n");
  printf("[[0.00122331, -0.00700767, 0.0082592],[0.00805871, -0.00893597],"
         "[-0.00121621]]\n");
         
  printf("C Values\n");
  printf("[%g, %g, %g]\n",
         displacement[0], displacement[1], displacement[2]);
  printf("[[%g, %g, %g],[%g, %g],[%g]]\n",
         strain[0], strain[1], strain[2], strain[3], strain[4], strain[5]); 
}
