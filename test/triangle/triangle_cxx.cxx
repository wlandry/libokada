#include "../../src/okada/Triangle_Fault.hxx"
#include "../../src/okada/invert_z.hxx"
#include <fstream>
#include <cstdlib>
int main(int argc, char *argv[])
{
  const FTensor::Tensor1<double,3> origin(3.14,1.5,9.2);
  const FTensor::Tensor1<double,3> u(6.5,3.5,9.2);
  const FTensor::Tensor1<double,3> v(-7.1,8.2,8.1);
  const FTensor::Tensor1<double,3> jump(8.2,-4.6,-2.2);
  okada::Triangle_Fault triangle(origin,u,v,jump,1,1);

  if(argc!=2)
    {
      std::cerr << "Need a filename to read\n";
      std::exit(0);
    }
  okada::Displacement absolute_diff, relative_diff;
  FTensor::Index<'i',3> i;
  FTensor::Index<'j',3> j;
  absolute_diff.first(i)=0;
  absolute_diff.second(i,j)=0;
  relative_diff.first(i)=0;
  relative_diff.second(i,j)=0;

  std::ifstream input(argv[1]);
  FTensor::Tensor1<double,3> xyz;
  okada::Displacement d;
  input >> xyz >> d.first >> d.second;
  while(input)
    {
      okada::Displacement disp(triangle.displacement(okada::invert_z(xyz)));
      disp.first = okada::invert_z(disp.first);
      disp.second = okada::invert_z(disp.second);
      for(int i=0;i<3;++i)
        {
          absolute_diff.first(i)=std::max(absolute_diff.first(i),
                                          std::abs(disp.first(i)-d.first(i)));
          relative_diff.first(i)=
            std::max(relative_diff.first(i),
                     2*std::abs(disp.first(i)-d.first(i))/
                     (std::abs(disp.first(i))+std::abs(d.first(i))));
          for(int j=i;j<3;++j)
            {
              absolute_diff.second(i,j)=std::max(absolute_diff.second(i,j),
                                                 std::abs(disp.second(i,j)-d.second(i,j)));
              relative_diff.second(i,j)=
                std::max(relative_diff.second(i,j),
                         2*std::abs(disp.second(i,j)-d.second(i,j))/
                         (std::abs(disp.second(i,j))+std::abs(d.second(i,j))));
            }
        }
      input >> xyz >> d.first >> d.second;
    }
  std::cout << "Displacement: Absolute Difference: "
            << absolute_diff.first << "\n\t"
            << "Relative Difference: " << relative_diff.first << "\n"
            << "Strain: Absolute Difference: "
            << absolute_diff.second << "\n\t"
            << "Relative Difference: " << relative_diff.second << "\n";
}

