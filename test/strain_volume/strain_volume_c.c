#include "../../src/okada_c.h"

#include <stdio.h>


int main(int argc, char *argv[])
{
  double lambda=2.3, mu=2.3, strike=30.0;
  double origin[3], shape[3], coord[3], displacement[3], applied_strain[6],
    output_strain[6];
  origin[0]=3.14;
  origin[1]=1.5;
  origin[2]=9.2;
  shape[0]=6.5;
  shape[1]=3.5;
  shape[2]=9.2;
  coord[0]=9.5;
  coord[1]=-8.5;
  coord[2]=8;
  applied_strain[0]=1.1;
  applied_strain[1]=1.2;
  applied_strain[2]=1.3;
  applied_strain[3]=1.4;
  applied_strain[4]=1.5;
  applied_strain[5]=1.6;
  strain_volume_displacement(origin,shape,strike,applied_strain,lambda,mu,coord,
                             displacement,output_strain);

  printf("Matlab Values\n");
  printf("[0.0482885, -0.588411, -0.615505]\n");
  printf("[[0.0156761, 0.0360055, 0.0224823], [-0.055278, -0.0334835], "
         "[0.0331892]]\n");
         
  printf("C Values\n");
  printf("[%g, %g, %g]\n",
         displacement[0], displacement[1], displacement[2]);
  printf("[[%g, %g, %g],[%g, %g],[%g]]\n",
         output_strain[0], output_strain[1], output_strain[2],
         output_strain[3], output_strain[4], output_strain[5]); 
}
