#include "../../src/okada_c.h"

#include <stdio.h>


int main(int argc, char *argv[])
{
  double lambda=1.2,
    mu=2.3,
    slip=-10,
    opening=3,
    width=.25,
    length=.2,
    strike=30,
    dip=75,
    rake=10;
  double origin[3], coord[3], displacement[3], strain[6];
  origin[0]=-0.1001;
  origin[1]=-0.05001;
  origin[2]=0;
  coord[0]=0.27000000000000002;
  coord[1]=0.45999999999999996;
  coord[2]=0.0000000000000000;
  okada_displacement(lambda,mu,slip,opening,width,length,strike,dip,rake,
                     origin,coord,displacement,strain);

  printf("Original Fortran Values\n");
  printf("[-0.11703878883185220, -0.24255758317142168, "
         "-5.3610656410455704E-002]\n");
  printf("[[0.35701698586819242, 0.53027513711779950, 0.0000000000000000],"
         "[0.57538928687097957, 0.0000000000000000],[-0.19291162490844727]]\n");
         
  printf("libokada C Values\n");
  printf("[%.17g, %.17g, %.17g]\n",
         displacement[0], displacement[1], displacement[2]);
  printf("[[%.17g, %.17g, %.17g],[%.17g, %.17g],[%.17g]]\n",
         strain[0], strain[1], strain[2], strain[3], strain[4], strain[5]); 
}
