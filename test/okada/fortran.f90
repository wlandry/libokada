program fortran

  implicit none
  integer i,j,n
  real*8 size, dx
  real*8 lambda, mu, slip, opening, width, length, strike, dipd, rake, origin(3)
  real*8 x,y,z,disp(3),deriv(3,3),strain(3,3)

  n=100
  size=1
  dx=size/n

  lambda=1.2
  mu=2.3
  slip=-10
  opening=3
  width=.25
  length=.2
  strike=30
  dipd=75
  rake=10
  origin(1)=-0.1001;
  origin(2)=-0.05001;
  origin(3)=0;

  do i=0,n
     do j=0,n
        x=-0.5+i*dx
        y=-0.5+j*dx
        z=0
        call okada(lambda,mu,slip,opening,origin(1),origin(2),origin(3),&
             width,length,strike,dipd,rake,x,y,z,disp,deriv)
        strain=deriv
        strain(1,2)=(deriv(1,2)+deriv(2,1))/2
        strain(1,3)=(deriv(1,3)+deriv(3,1))/2
        strain(2,3)=(deriv(2,3)+deriv(3,2))/2
        write(*,*) "[",x,",",y,",",z,"]"
        write(*,*) "[",disp(1),",",disp(2),",",disp(3),"]"
        write(*,*) "[[",strain(1,1),",",strain(1,2),",",strain(1,3),"],[",&
             strain(2,2),",",strain(2,3),"],[",strain(3,3),"]]"
     end do
  end do

end program fortran
