!----------------------------------------------------------------------------
!> Subroutine Okada
!! converts Aki's to Okada's fault representation and evaluates the
!! displacement at position (xrec,yrec,zrec) due to a series of faults 
!! with given slip, lengths, widths, rake, etc...
!!
!! lambda, mu = the two Lame constants in Pascal (SI unit)
!! (xs,ys,zs) = coordinates of the start point of strike
!! with x = north, y = east, z = downward.
!! all angles in degree.
!! (xrec,yrec,zrec) = cartesian coordinates of observations
!! disp = 3 displacement components: ux,uy,uz
!!
!!   \author R. Wang              - Created, Potsdam, Nov, 2001
!!   \author S. Barbot (05/07/10) - Updated to Fortran90
!----------------------------------------------------------------------------
SUBROUTINE okada(lambda,mu,slip,opening,x1,x2,x3, &
     width,length,strike,dipd,rake, &
     xrec,yrec,zrec,disp,deriv)
  IMPLICIT NONE
  REAL*8, INTENT(IN) :: lambda,mu,slip,opening
  REAL*8, INTENT(IN) :: x1,x2,x3,width,length,strike,dipd,rake
  REAL*8, INTENT(IN) :: xrec,yrec,zrec
  REAL*8, INTENT(OUT) :: disp(3), deriv(3,3)

  ! from Okada's subroutine DC3D0:
  INTEGER IRET
  REAL*4 ALPHA,X,Y,Z,DEPTH,DIP,POT3,POT4,& 
       UX,UY,UZ,UXX,UYX,UZX,UXY,UYY,UZY,UXZ,UYZ,UZZ

  ! more from Okada's subroutine DC3D:
  REAL*4 AL1,AL2,AW1,AW2,DISL1,DISL2,DISL3

  ! LOCAL CONSTANTS
  REAL*8 degtorad
  PARAMETER(degtorad=1.745329252E-02)

  REAL*8 st,di,ra
  REAL*8 csst,ssst,csra,ssra,csdi,ssdi

  ! receiver and source independent variables
  ALPHA=sngl((lambda+mu)/(lambda+2.d0*mu))
  POT3=0.0
  POT4=0.0
  DISL3=0.0
  AL1=0.0
  AW2=0.0
  Z=REAL(-zrec)

  ! initialization
  disp(:)=0.d0

  st=strike*degtorad
  csst=dcos(st)
  ssst=dsin(st)

  di=dipd*degtorad
  csdi=dcos(di)
  ssdi=dsin(di)

  ra=rake*degtorad
  csra=dcos(ra)
  ssra=dsin(ra)

  ! transform from Aki's to Okada's system
  X=sngl((xrec-x1)*csst+(yrec-x2)*ssst)
  Y=sngl((xrec-x1)*ssst-(yrec-x2)*csst)
  DEPTH=sngl(x3)
  DIP=sngl(dipd)

  ! finite source
  AL2=sngl(length)
  AW1=-sngl(width)
  DISL1=sngl(slip*csra)
  DISL2=sngl(slip*ssra)
  DISL3=sngl(opening)

  IRET=1
  CALL DC3D(ALPHA,X,Y,Z,DEPTH,DIP,AL1,AL2,AW1,AW2, &
       DISL1,DISL2,DISL3,UX,UY,UZ, &
       UXX,UYX,UZX,UXY,UYY,UZY,UXZ,UYZ,UZZ,IRET)

  IF (IRET .EQ. 0) THEN
     ! transform from Okada's to Aki's system
     disp(1)=dble(UX)*csst+dble(UY)*ssst
     disp(2)=dble(UX)*ssst-dble(UY)*csst
     disp(3)=-dble(UZ)
     deriv(1,1)=dble(UXX)*(csst**2) + dble(UYX + UXY)*csst*ssst &
          + dble(UYY)*(ssst**2)

     deriv(1,2)=dble(UXX)*csst*ssst - dble(UXY)*(csst**2) &
          + dble(UYX)*(ssst**2) - dble(UYY)*csst*ssst
     deriv(1,3)=-dble(UXZ)*csst - dble(UYZ)*ssst
     deriv(2,1)=dble(UXX)*csst*ssst + dble(UXY)*(ssst**2) &
          - dble(UYX)*(csst**2) - dble(UYY)*csst*ssst
     deriv(2,2)=dble(UXX)*(ssst**2) - dble(UXY + UYX)*csst*ssst &
          + dble(UYY)*(csst**2)
     deriv(2,3)=dble(UYZ)*csst - dble(UXZ)*ssst
     deriv(3,1)=-dble(UZX)*csst - dble(UZY)*ssst
     deriv(3,2)=dble(UZY)*csst - dble(UZX)*ssst
     deriv(3,3)=dble(UZZ)

  ELSE
     ! singular point at fault edge
     disp=0._8
  ENDIF

END SUBROUTINE okada
